# Random List

* Reorder items on your list

# Enter the List

* Make a list in list.txt
* Separate each item with line breaks

# Randomize your List

* Run the program - python random-list.py in the terminal
* The list in list.txt is now randomized

# Runs in Python 2.7

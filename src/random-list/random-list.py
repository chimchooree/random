import random

## Enter a list in list.txt, one item per line, and I'll randomize it.

# Fill file with items of list, one per line
def rewrite(list_file, lines):
    output = open(list_file,'a')
    for i in range(len(lines)):
        output.writelines(lines[i] + '\n')

# Randomly reorder items on given list
def randomize(lines):
    return random.sample(lines, len(lines))

# Empty list.txt
def clear_file(list_file):
    open(list_file, 'w').close()

# Convert list.txt to list
def make_list(list_file):
    lines = list()
    with open(list_file, "r") as f:
        lines = f.read().splitlines()
    return lines

# Flow of Program
def main():
    list_file = 'list.txt' # File containing your list
    lines = make_list(list_file)
    clear_file(list_file)
    lines = randomize(lines)
    rewrite(list_file, lines)

## Start Program ##

main()

import random

## I'll choose your 6 squares from a 3x3 grid.

total = 9 # total squares in grid
side = 3 # squares per side

def translate(squares):
    index = 0
    for i in squares:
        if index == 2 or index == 6:
            squares = squares[:index+1] + '\n' + squares[index+1:]
        index += 1
    return squares

def nine():
    count = 0
    o = []
    squares = ""
    for square in range(3):
        rand = random.randint(0,8)
        while rand in o or rand not in [0,1,2,3,4,5,6,7,8]:
            rand = random.randint(0,8)
        o.append(rand)
            
    for j in range(total):
        if count in o:
            squares += "o"
        else:
            squares += "x"
        count += 1

    return squares

# Flow of Program
def main():
    print("x marks the spot!")
    print(translate(nine()))

## Start Program ##

main()

# Scratchcard Solver

* Randomly choose 6 squares from a 3x3 grid, so fate can scratch up your scratchcard.

# Instructions

* Buy a scratchcard
* Run the application
* Scratch your card

# Runs in Python 2.7

# Random Tools

## Random List

* Randomly reorder the items on your list

## Scratchcard Solver

* Randomly choose 6 squares from a grid of 9
* Won a jackpot on its first run :)

![Verpets jackpot screenshot](https://i.imgur.com/WymKVnS.png)

# Runs in Python 2.7
